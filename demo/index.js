const { Validator } = require('../dist')
const { runSpec } = require('@util/tinytest')

/* for demo purposes */
const fooPack = new Map();
fooPack.set('@name', 'fooPack');
fooPack.set('fooLike', input => input === 'foo')
fooPack.set('barLike', input => input === 'bar');

const truthy = new Validator({ delimiter: '::' })
truthy.setRule('isFoo', (input) => input === 'foo')
truthy.setRule('isBar', (input) => input === 'bar')
truthy.setAlias('isValidPass', [
  'min-len 3',
  'max-len 75',
  'specials 2',
  'capitals 1',
  'numbers 2',
])

console.log('==============================================================')

runSpec({
  name: 'Simple -> false',
  test: truthy.validate('foo', 'isBar'),
  expect: false,
})

runSpec({
  name: 'Simple -> true',
  test: truthy.validate('foo', 'isFoo'),
  expect: true,
})

console.log('==============================================================')

runSpec({
  name: 'List -> true',
  test: truthy.validate('bar', ['isBar']),
  expect: true,
})

runSpec({
  name: 'List -> false',
  test: truthy.validate('bar', ['isFoo', 'isBar']),
  expect: false,
})

console.log('==============================================================')

runSpec({
  name: 'Composite (all), 1 param -> true',
  test: truthy.all(true),
  expect: true,
})

runSpec({
  name: 'Composite (all), 2 param -> true',
  test: truthy.all(true, true),
  expect: true,
})

runSpec({
  name: 'Composite (all), 3 param -> true',
  test: truthy.all(true, true, true),
  expect: true,
})

console.log('==============================================================')

runSpec({
  name: 'Composite (all), 1 param -> false',
  test: truthy.all(false),
  expect: false,
})

runSpec({
  name: 'Composite (all), 2 param -> false',
  test: truthy.all(false, false),
  expect: false,
})

runSpec({
  name: 'Composite (all), 3 param -> false',
  test: truthy.all(false, false, false),
  expect: false,
})

console.log('==============================================================')

const form = {
  firstName: 'foo',
  lastName: 'bar',
  email: 'foo.bar@foobarmail.com',
}

runSpec({
  name: 'Complex, 2 param -> true',
  test: truthy.validate(form, ['firstName::isFoo', 'lastName::isBar']),
  expect: true,
})

runSpec({
  name: 'Complex, 3 param -> false',
  test: async () => truthy.validate(form, [
      'firstName::isFoo',
      'lastName::isBar',
      'firstName::isBar',
  ]),
  expect: false,
})

console.log('==============================================================')

runSpec({
  name: 'Async function runner Primitive -> true',
  test: async () => true,
  expect: true,
})

const asyncThing = async (result, seconds = 1) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(result)
    }, seconds * 1000)
  })
}

runSpec({
  name: 'Async form -> true firstName::isFoo',
  test: truthy.validate(asyncThing(form), 'firstName::isFoo'),
  expect: true,
})

runSpec({
  name: 'Async -> true foo=isFoo',
  test: truthy.validate(asyncThing('foo'), 'isFoo'),
  expect: true,
})

// Only providing this test to make clear that runSpec is testing asynchronously
truthy
  .validate(asyncThing('foo'), 'isFoo')
  .then((result) => {
    runSpec({
      name: 'Ugly Promise -> true foo=isFoo',
      test: result,
      expect: true,
    })
  })
  .catch((e) => console.log('error'))

runSpec({
  name: 'Rulepack test fooPack::fooLike',
  test: truthy.validate('foo', 'fooPack::fooLike'),
  expect: true,
})
