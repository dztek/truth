const { runspec } = require('@logicos/tinytest')
const { Cloud } = require('../dist')

const truthy = new Cloud({
  provider: 'http',
  config: {
    baseUrl: 'http://localhost:5000/dz-services/us-central1',
  },
})

runspec({
  name: 'Is Active',
  test: truthy.logic('000-abc123-000', ['accountActive']),
  expect: true,
})
