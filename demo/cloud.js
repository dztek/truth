const { runspec } = require('@logicos/tinytest');
const { Cloud } = require('../dist');

runspec({
  name: 'Init test',
  test: true,
  expect: true,
});

// Requires the cloud functions to be running!
const truthy = new Cloud({
  provider: 'http',
  namespace: '',
  config: {
    baseUrl: 'http://localhost:9999/@truthy'
  }
});

runspec({
  name: 'Simple > true',
  test: truthy.logic(2, ['isEven']),
  expect: true,
});

runspec({
  name: 'Simple > false',
  test: truthy.logic(2, ['isOdd']),
  expect: false,
});

runspec({
  name: 'List > false',
  test: truthy.logic(3, ['isMultOf4', 'isOdd']),
  expect: false,
});

runspec({
  name: 'List > true',
  test: truthy.logic(16, ['isEven', 'isMultOf4']),
  expect: true,
});
