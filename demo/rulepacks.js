const { Validator } = require('../dist')
const { runSpec } = require('@util/tinytest')

/* for demo purposes */
const fooPack = new Map();
fooPack.set('@name', 'fooPack');
fooPack.set('fooLike', input => input === 'foo')
fooPack.set('barLike', input => input === 'bar');

const truthy = new Validator({ delimiter: '::' })

