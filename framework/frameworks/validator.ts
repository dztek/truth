import { flatten } from 'lodash';

interface IValidatorOptions {
  delimiter: string,
  link?: object[],
  packs?: Map<string, Function>[],
  sep?: string,
  separator?: string,
}

type RuleName = string;
type RuleSet = Function[];
type ProvidedRules = RuleName | RuleName[];

export class Validator {
  private delimiter: IValidatorOptions['delimiter'] = '.';
  private mode: string = 'truthy'; // truthy | message | explosive
  private rules: Map<string, Function> = new Map();
  private aliases: Set<string> = new Set();

  constructor(opts = {} as IValidatorOptions) {
    // console.log({ opts });
    this.delimiter = opts.separator || opts.delimiter || '.';
    // this._registerPacks();
    // this._makeLinks(opts.link);
  }
  _isArray(arr) {
    return Array.isArray(arr);
  }
  _isObject(obj) {
    return Object.prototype.toString.call(obj) === '[object Object]';
  }
  _isAsyncConstructor(input) {
    return input?.constructor?.name === 'AsyncFunction';
  }
  _isAsync(input) {
    return typeof input?.then === 'function';
  }
  _makeLinks(constructors = []) {
    console.log({ constructors, method: '_makeLinks', this: this, });
  }
  _registerPacks(packList = []) {
    const ruleList = [...this.rules, ...packList];
    this.rules = new Map(ruleList);
  }
  all(...validations) {
    return flatten(validations).every(test => test === true);
    // return [...validations].reduce((a, b) => a ? (b ? true : false) : a, true);
  }
  getRule(name = '') {
    return this.rules.get(name) || this.returnFalse;
  }
  returnFalse() {
    return false;
  }
  setRule(name = '', handler = null): boolean | undefined {
    if(!handler) return false;

    this.rules.set(name, handler);
  }
  setAlias(name = null): boolean {
    if (!name) return false;

    this.aliases.add(name);

    return true;
  }
  separator(chars = this.delimiter) {
    this.delimiter = chars;

    return this.delimiter;
  }
  logic(input: any, rule: [] | string = []) {

  }
  _loadRuleSetsByName(rule): RuleSet {
    return this._normalizeRules(rule).map(name => this.getRule(name));
  }
  _normalizeRules(rule: RuleName | RuleName[]): RuleName[] {
    return Array.isArray(rule) ? rule : [rule];
  }
  validate(input: any, rule: ProvidedRules = []) {
    const rules = this._normalizeRules(rule);
    const ruleSets = this._loadRuleSetsByName(rule);

    if (ruleSets.some(this._isAsyncConstructor)) {
      return this.truthyAsyncRuleValidate(input, rule);
    }
    if (this._isAsync(input)) {
      return this.truthyAsyncValidate(input, rule);
    }

    if (this._isArray(input)) {
      const atLeastOneAsyncInput = input.some(this._isAsync);
      if (atLeastOneAsyncInput) {
        return (async () => {
          const asyncResults = await Promise.all(input.map(singleInput => this.validate(singleInput, rule)));
          // NOTE: Why does this require destructing? Should accept and array for better performance
          return this.all(...asyncResults);
        })();
      } else {
        return this.all(input.map(singleInput => this.validate(singleInput, rule)));
      }
    }

    if (this._isObject(input)) {
      return this.truthyValidateMap(input, rules);
    }

    // Base validator for each mode
    if (this.mode === 'truthy') {
      return this.truthyValidate(input, ruleSets);
    }
  }
  async truthyAsyncRuleValidate(input: unknown, rule: ProvidedRules) {
    // NO PROTECTION for rules that fail to load, they're ignored (possible break in logic - should assert count first)
    const eventualInput = await input;
    const subs = await Promise.all(
      this._loadRuleSetsByName(rule).map(async validate => await validate(eventualInput))
    );
    // console.log('>', subs, this.all(subs));
    return this.all(subs);
  }
  async truthyAsyncValidate(input: unknown | Promise<unknown>, rule: ProvidedRules) {
    // This will run the validator with a resolved input instead of a promise
    return this.validate(await input, rule);
  }
  truthyValidateMap(input: object, rules: string[] = []): boolean {
    const validations: boolean[] = [];
    for (const ruleStr of rules) {
      const [key, ruleName] = ruleStr.split(this.delimiter);
      const mapValue = input[key];

      validations.push(this.validate(mapValue, ruleName));
    }
    return validations.every(one => one === true);
  }
  truthyValidate(input: string, runners): boolean {
    return this.all(...runners.map(run => run(input)));
  }
}
