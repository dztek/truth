import axios from 'axios';
import { kebabCase } from 'lodash';
import { Validator } from './validator';

interface IHTTPResponse {
  data?: any;
  [key:string]: any;
}

interface ICloudOptions {
  provider?: CloudMode;
  ref?: string;
  config?: {
    [key: string]: any
  }
}
interface IRealmConfig {
  namespace: string;
  port: number;
}
interface IFirebaseConfig {
  name: string
}
enum CloudMode {
  Http = 'http',
  Realm = 'realm',
}
export class Cloud {
  private _mode: CloudMode;
  private providerConfig: ICloudOptions;
  private truthy: Validator;

  constructor(config: ICloudOptions) {
    // @TODO: use decorator to protect getter/setter and make enum
    this.mode = config.provider;
    this.providerConfig = config;
    this.truthy = new Validator();
  }
  init(config: ICloudOptions) {
    console.log('Initializing');
  }
  async validateWithHttp(input: any, locations: string[]) {
    let requests;
    
    try {
      requests = locations
        .map(async fn => await axios.post(fn, `${input}`).catch(_e => {
      // @NOTE: ignoring errors in truthy mode use other mode for more verbose failure
      // @NOTE: Also use best practice of FORCE TRUE to make sure connection errors aren't misunderstood as logic
        }));
    } catch(_e) {
      // FAIL_QUIETLY see notes on this topic
    }
    
    const results = await Promise.all(requests);

    return this.truthy.all(...results.map((result: IHTTPResponse) => result && result.data));
  }
  async logic(input: any, rule: string | [] = []) {
    const rules = Array.isArray(rule) ? rule : [rule];
    // const logicCall;

    if (this.mode === 'http') {
      const locations: string[] = rules.map(name => `${this.providerConfig.config.baseUrl}/${name}`);
      return this.validateWithHttp(input, locations);
    }
    if (this.mode === 'realm') {
      const locations: string[] = rules.map(name => `${this.providerConfig.config.baseUrl}/${kebabCase(name)}`);
      return this.validateWithHttp(input, locations);
    }
    // return this.truthy.validate(input, rule);
  }
  get mode() {
    return this._mode;
  }
  set mode(mode) {
    this._mode = mode ? mode : this._mode;
  }
}