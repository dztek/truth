"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __spreadArray = (this && this.__spreadArray) || function (to, from, pack) {
    if (pack || arguments.length === 2) for (var i = 0, l = from.length, ar; i < l; i++) {
        if (ar || !(i in from)) {
            if (!ar) ar = Array.prototype.slice.call(from, 0, i);
            ar[i] = from[i];
        }
    }
    return to.concat(ar || Array.prototype.slice.call(from));
};
exports.__esModule = true;
exports.Validator = void 0;
var Validator = /** @class */ (function () {
    function Validator(opts) {
        if (opts === void 0) { opts = {}; }
        this.delimiter = '::';
        this.mode = 'truthy'; // truthy | message | explosive
        this.rules = new Map();
        this.aliases = new Set();
        this._registerPacks(opts.packs);
        // this._makeLinks(opts.link);
    }
    Validator.prototype._isObject = function (obj) {
        return Object.prototype.toString.call(obj) === '[object Object]';
    };
    Validator.prototype._isAsync = function (input) {
        return typeof input.then === 'function';
    };
    Validator.prototype._makeLinks = function (constructors) {
        if (constructors === void 0) { constructors = []; }
        console.log({ constructors: constructors });
    };
    Validator.prototype._registerPacks = function (packList) {
        if (packList === void 0) { packList = []; }
        // this.rules = new Map([...this.rules, ...[...packList])
    };
    Validator.prototype.all = function () {
        var validations = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            validations[_i] = arguments[_i];
        }
        return __spreadArray([], validations, true).reduce(function (a, b) { return a ? (b ? true : false) : a; }, true);
    };
    Validator.prototype.getRule = function (name) {
        if (name === void 0) { name = null; }
        return this.rules.get(name) || this.returnFalse;
    };
    Validator.prototype.returnFalse = function () {
        return false;
    };
    Validator.prototype.setRule = function (name, handler) {
        if (name === void 0) { name = null; }
        if (handler === void 0) { handler = null; }
        if (!handler)
            return false;
        this.rules.set(name, handler);
    };
    Validator.prototype.setAlias = function (name) {
        if (name === void 0) { name = null; }
        if (!name)
            return false;
        this.aliases.add(name);
        return true;
    };
    Validator.prototype.separator = function (chars) {
        if (chars === void 0) { chars = this.delimiter; }
        this.delimiter = chars;
        return this.delimiter;
    };
    Validator.prototype.logic = function (input, rule) {
        if (rule === void 0) { rule = []; }
    };
    // @TODO: cannot specify conditonal type here b/c TS complains everywhere else where type is known
    Validator.prototype.validate = function (input, rule) {
        var _this = this;
        if (rule === void 0) { rule = []; }
        var rules = Array.isArray(rule) ? rule : [rule];
        var ruleSets = rules.map(function (name) { return _this.getRule(name); });
        if (this._isAsync(input)) {
            return this.truthyAsyncValidate(input, rule);
        }
        if (this._isObject(input)) {
            return this.truthyValidateMap(input, rules);
        }
        // Base validator for each mode
        if (this.mode === 'truthy') {
            return this.truthyValidate(input, ruleSets);
        }
    };
    Validator.prototype.truthyAsyncValidate = function (eventualInput, rule) {
        return __awaiter(this, void 0, void 0, function () {
            var input;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, eventualInput];
                    case 1:
                        input = _a.sent();
                        return [2 /*return*/, this.validate(input, rule)];
                }
            });
        });
    };
    Validator.prototype.truthyValidateMap = function (input, rules) {
        var _this = this;
        if (rules === void 0) { rules = []; }
        return this.all.apply(this, rules.map(function (r) {
            var _a = r.split(_this.delimiter).reverse(), ruleName = _a[0], inputKey = _a[1];
            // @TODO: Maybe support rule groups in validation maps just like in main validate method
            if (inputKey) {
                return _this.truthyValidate(input[inputKey], [_this.getRule(ruleName)]);
            }
            // @TODO: Maybe support all-key validation, not sure yet.
            return false;
        }));
    };
    Validator.prototype.truthyValidate = function (input, runners) {
        return this.all.apply(this, runners.map(function (run) { return run(input); }));
    };
    return Validator;
}());
exports.Validator = Validator;
