const { Validator } = require('../dist')
const { runSpec } = require('@util/tinytest')

const truthy = new Validator({ delimiter: '::' })
truthy.setRule('required', (input) => typeof input !== 'undefined')


console.log('==============================================================')

runSpec({
  name: 'required string',
  test: truthy.validate('foo', 'required'),
  expect: true,
})
runSpec({
  name: 'required (empty) string',
  test: truthy.validate('', 'required'),
  expect: true,
})
runSpec({
  name: 'undefined should be false',
  test: truthy.validate(undefined, 'required'),
  expect: false,
})

const form = { a: 'exists' };
runSpec({
  name: 'missing prop should be false',
  test: truthy.validate(form.b, 'required'),
  expect: false,
})
