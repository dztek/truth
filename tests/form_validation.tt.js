const { Validator } = require('../dist')
const { runSpec, test } = require('@util/tinytest')
const { isEmail } = require('validator')

const truthy = new Validator({ delimiter: '->' })

truthy.setRule('required', (input) => typeof input !== 'undefined')
truthy.setRule('isEmail', isEmail)
truthy.setRule('over40', (input) => +input > 40)
truthy.setRule('emailOkay', async (input) => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(input?.endsWith('mailinator.com'));
    }, 4000)
  })
})

console.log('==============================================================')
const form = { email: 'bob@mailinator.com', username: 'bob', age: 42 }

async function eventually(value, sec = 1) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(value)
    }, sec * 1000)
  })
}

test(truthy.validate(55, 'over40'), true, 'over40 direct')
test(truthy.validate(form.email, 'isEmail'), true, 'isEmail direct')
test(truthy.validate([55,66,77], 'over40'), true, 'over40 list')
test(truthy.validate(form, ['age->over40', 'email->isEmail']), true, 'over40 with valid email')
test(truthy.validate(form, ['age->over40', 'email->isEmail']), true, 'over40 with valid async email')

runSpec({
  name: 'async validator emailOkay',
  test: truthy.validate(form.email, 'emailOkay'),
  expect: true,
})

runSpec({
  name: 'async validator and email for emailOkay',
  test: truthy.validate(eventually(form.email, 1), 'emailOkay'),
  expect: true,
})

runSpec({
  name: 'async validator and email for emailOkay (fail from bad email)',
  test: truthy.validate(eventually('foo', 1), 'emailOkay'),
  expect: false,
})

runSpec({
  name: 'async validator and email for emailOkay (fail from no email)',
  test: truthy.validate(eventually(undefined, 1), 'emailOkay'),
  expect: false,
})

runSpec({
  name: 'sync validation isEmail (true)',
  test: truthy.validate(form.email, 'isEmail'),
  expect: true,
})

runSpec({
  name: 'sync validation',
  test: truthy.validate(form.email, 'isEmail'),
  expect: true,
})

runSpec({
  name: 'email.isEmail',
  test: truthy.validate(form, 'email->isEmail'),
  expect: true,
})


runSpec({
  name: 'username.required',
  test: truthy.validate(form, 'username->required'),
  expect: true,
})

runSpec({
  name: 'age.over40 (true)',
  test: truthy.validate(form, 'age->over40'),
  expect: true,
})
