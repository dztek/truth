var $g5Y9E$axios = require("axios");
var $g5Y9E$lodash = require("lodash");

function $parcel$export(e, n, v, s) {
  Object.defineProperty(e, n, {get: v, set: s, enumerable: true, configurable: true});
}
function $parcel$interopDefault(a) {
  return a && a.__esModule ? a.default : a;
}

$parcel$export(module.exports, "Cloud", () => $f6cfbcf333f088b9$export$e2767e27b246eae1);
$parcel$export(module.exports, "Validator", () => $8805bf28b09aa68b$export$9eeb22c0bba4ed5e);



class $8805bf28b09aa68b$export$9eeb22c0bba4ed5e {
    constructor(opts = {}){
        this.delimiter = ".";
        this.mode = "truthy" // truthy | message | explosive
        ;
        this.rules = new Map();
        this.aliases = new Set();
        // console.log({ opts });
        this.delimiter = opts.separator || opts.delimiter || ".";
    // this._registerPacks();
    // this._makeLinks(opts.link);
    }
    _isArray(arr) {
        return Array.isArray(arr);
    }
    _isObject(obj) {
        return Object.prototype.toString.call(obj) === "[object Object]";
    }
    _isAsyncConstructor(input) {
        return input?.constructor?.name === "AsyncFunction";
    }
    _isAsync(input) {
        return typeof input?.then === "function";
    }
    _makeLinks(constructors = []) {
        console.log({
            constructors: constructors,
            method: "_makeLinks",
            this: this
        });
    }
    _registerPacks(packList = []) {
        const ruleList = [
            ...this.rules,
            ...packList
        ];
        this.rules = new Map(ruleList);
    }
    all(...validations) {
        return (0, $g5Y9E$lodash.flatten)(validations).every((test)=>test === true);
    // return [...validations].reduce((a, b) => a ? (b ? true : false) : a, true);
    }
    getRule(name = "") {
        return this.rules.get(name) || this.returnFalse;
    }
    returnFalse() {
        return false;
    }
    setRule(name = "", handler = null) {
        if (!handler) return false;
        this.rules.set(name, handler);
    }
    setAlias(name = null) {
        if (!name) return false;
        this.aliases.add(name);
        return true;
    }
    separator(chars = this.delimiter) {
        this.delimiter = chars;
        return this.delimiter;
    }
    logic(input, rule = []) {}
    _loadRuleSetsByName(rule) {
        return this._normalizeRules(rule).map((name)=>this.getRule(name));
    }
    _normalizeRules(rule) {
        return Array.isArray(rule) ? rule : [
            rule
        ];
    }
    validate(input, rule = []) {
        const rules = this._normalizeRules(rule);
        const ruleSets = this._loadRuleSetsByName(rule);
        if (ruleSets.some(this._isAsyncConstructor)) return this.truthyAsyncRuleValidate(input, rule);
        if (this._isAsync(input)) return this.truthyAsyncValidate(input, rule);
        if (this._isArray(input)) {
            const atLeastOneAsyncInput = input.some(this._isAsync);
            if (atLeastOneAsyncInput) return (async ()=>{
                const asyncResults = await Promise.all(input.map((singleInput)=>this.validate(singleInput, rule)));
                // NOTE: Why does this require destructing? Should accept and array for better performance
                return this.all(...asyncResults);
            })();
            else return this.all(input.map((singleInput)=>this.validate(singleInput, rule)));
        }
        if (this._isObject(input)) return this.truthyValidateMap(input, rules);
        // Base validator for each mode
        if (this.mode === "truthy") return this.truthyValidate(input, ruleSets);
    }
    async truthyAsyncRuleValidate(input, rule) {
        // NO PROTECTION for rules that fail to load, they're ignored (possible break in logic - should assert count first)
        const eventualInput = await input;
        const subs = await Promise.all(this._loadRuleSetsByName(rule).map(async (validate)=>await validate(eventualInput)));
        // console.log('>', subs, this.all(subs));
        return this.all(subs);
    }
    async truthyAsyncValidate(input, rule) {
        // This will run the validator with a resolved input instead of a promise
        return this.validate(await input, rule);
    }
    truthyValidateMap(input, rules = []) {
        const validations = [];
        for (const ruleStr of rules){
            const [key, ruleName] = ruleStr.split(this.delimiter);
            const mapValue = input[key];
            validations.push(this.validate(mapValue, ruleName));
        }
        return validations.every((one)=>one === true);
    }
    truthyValidate(input, runners) {
        return this.all(...runners.map((run)=>run(input)));
    }
}


var $f6cfbcf333f088b9$var$CloudMode;
(function(CloudMode) {
    CloudMode["Http"] = "http";
    CloudMode["Realm"] = "realm";
})($f6cfbcf333f088b9$var$CloudMode || ($f6cfbcf333f088b9$var$CloudMode = {}));
class $f6cfbcf333f088b9$export$e2767e27b246eae1 {
    constructor(config){
        // @TODO: use decorator to protect getter/setter and make enum
        this.mode = config.provider;
        this.providerConfig = config;
        this.truthy = new (0, $8805bf28b09aa68b$export$9eeb22c0bba4ed5e)();
    }
    init(config) {
        console.log("Initializing");
    }
    async validateWithHttp(input, locations) {
        let requests;
        try {
            requests = locations.map(async (fn)=>await (0, ($parcel$interopDefault($g5Y9E$axios))).post(fn, `${input}`).catch((_e)=>{
                // @NOTE: ignoring errors in truthy mode use other mode for more verbose failure
                // @NOTE: Also use best practice of FORCE TRUE to make sure connection errors aren't misunderstood as logic
                }));
        } catch (_e) {
        // FAIL_QUIETLY see notes on this topic
        }
        const results = await Promise.all(requests);
        return this.truthy.all(...results.map((result)=>result && result.data));
    }
    async logic(input, rule = []) {
        const rules = Array.isArray(rule) ? rule : [
            rule
        ];
        // const logicCall;
        if (this.mode === "http") {
            const locations = rules.map((name)=>`${this.providerConfig.config.baseUrl}/${name}`);
            return this.validateWithHttp(input, locations);
        }
        if (this.mode === "realm") {
            const locations = rules.map((name)=>`${this.providerConfig.config.baseUrl}/${(0, $g5Y9E$lodash.kebabCase)(name)}`);
            return this.validateWithHttp(input, locations);
        }
    // return this.truthy.validate(input, rule);
    }
    get mode() {
        return this._mode;
    }
    set mode(mode) {
        this._mode = mode ? mode : this._mode;
    }
}









//# sourceMappingURL=index.js.map
