# Truthie #
Validation Framework

## Why? ##
Truthie is a framework that makes any "truth evaluating" source queriable with text

## Getting Started ##
```js
// examples use tinytest
const { test } = require('@logicos/tinytest')
const truthie = require('truthie')
```
OR with ES6 modules
```js
import { test } from '@logicos/tinytest'
import * as truthie from 'truthie'
```
OR just grab what you need
```js
const { validate, validateAsync, use } = require('truthie')
```

## Examples ##

Direct validation:
```js
truthie.validate('one', 'min-len 2') === true
```

Async validation:
```js
truthie.validateAsync(afterAFew(), 'min-len 2') === true

// this just return Okay after few seconds
function afterAFew() {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve('Okay')
    }, 3000)
  })
}
```

Integrate validators:

Use validator packages
```js
const validator = require('validator')

truthie.use('validator', validator)

truthie.validate('joe@example.org', 'validator.isEmail')
```

Include validation rules
```js
const luhn = require('luhn')

truthie.rule('credit-card', luhn.validate)

truthie.validate('4111111111111111', 'credit-card') === true
```

